//
//  AuthService.swift
//  Calday
//
//  Created by d3byte on 27.01.2018.
//  Copyright © 2018 Pandinator. All rights reserved.
//

import UIKit

class AuthService: NSObject {

    class func isLoggedIn() -> Bool {
        let user = CoreDataService.getUser()
        if user != nil {
            return true
        }
        return false
    }
    
}
