//
//  CoreDataService.swift
//  Calday
//
//  Created by d3byte on 27.01.2018.
//  Copyright © 2018 Pandinator. All rights reserved.
//

import UIKit
import CoreData

class CoreDataService: NSObject {
    
    // MARK: Получаю контекст
    private class func getContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        return context
    }
    
    // MARK: Взаимодействия с пользователем
    class func getUser() -> User? {
        let context = getContext()
        var user: [User]?
        do {
            user = try context.fetch(User.fetchRequest())
            return user?[0]
        } catch {
            return user?[0]
        }
    }
    
    class func updateSubscriptionDate(date_subscribe: String) -> Bool {
        let context = getContext()
        var user: [User]? = nil
        do {
            user = try context.fetch(User.fetchRequest())
            guard let user = user?[0] else { return false }
            user.setValue(date_subscribe, forKey: "date_subscribe")
            return true
        } catch {
            return false
        }
    }
    
    class func editUser(email: String? = nil, first_name: String? = nil, last_name: String? = nil) -> Bool {
        let context = getContext()
        var user: [User]? = nil
        do {
            user = try context.fetch(User.fetchRequest())
            guard let user = user?[0] else { return false }
            if let email = email {
                user.setValue(email, forKey: "email")
            }
            if let first_name = first_name {
                user.setValue(first_name, forKey: "first_name")
            }
            if let last_name = last_name {
                user.setValue(last_name, forKey: "last_name")
            }
            return true
        } catch {
            return false
        }
    }
    
    class func saveUser(id: Int, date_register: String, date_subscribe: String, email: String, first_name: String, last_name: String, phone: String, role: Int) -> Bool {
        let context = getContext()
        let entity = NSEntityDescription.entity(forEntityName: "User", in: context)
        let manageObject = NSManagedObject(entity: entity!, insertInto: context)

        manageObject.setValue(id, forKey: "id")
        manageObject.setValue(date_register, forKey: "date_register")
        manageObject.setValue(date_subscribe, forKey: "date_subscribe")
        manageObject.setValue(email, forKey: "email")
        manageObject.setValue(first_name, forKey: "first_name")
        manageObject.setValue(last_name, forKey: "last_name")
        manageObject.setValue(phone, forKey: "phone")
        manageObject.setValue(role, forKey: "role")

        do {
            try context.save()
            return true
        } catch {
            return false
        }
    }
    
    class func removeUser() -> Bool {
        let context = getContext()
        let delete = NSBatchDeleteRequest(fetchRequest: User.fetchRequest())
        do {
            try context.execute(delete)
            return true
        } catch {
            return false
        }
    }
    
    // MARK: Взаимодействия с событиями
    class func getEvents() -> [Event]? {
        let context = getContext()
        var events: [Event]?
        do {
            events = try context.fetch(Event.fetchRequest())
            return events
        } catch {
            return events
        }
    }
    
    class func editEvent(id: Int, title: String? = nil, text: String? = nil, date: String? = nil, important: Bool? = nil, notifications: AnyObject? = nil, list_notifications: AnyObject? = nil) -> Bool {
        let context = getContext()
        let fetchRequest:NSFetchRequest<Event> = Event.fetchRequest()
        let predicate = NSPredicate(format: "id == %@", id)
        fetchRequest.predicate = predicate
        var events: [Event]? = nil
        do {
            events = try context.fetch(fetchRequest)
            guard let event = events?[0] else { return false }
            if let title = title {
                event.setValue(title, forKey: "title")
            }
            if let text = text {
                event.setValue(text, forKey: "text")
            }
            if let date = date {
                event.setValue(date, forKey: "date")
            }
            if let important = important {
                event.setValue(important, forKey: "important")
            }
            if let notifications = notifications {
                event.setValue(notifications, forKey: "notifications")
            }
            if let list_notifications = list_notifications {
                event.setValue(list_notifications, forKey: "list_notifications")
            }
            return true
        } catch {
            return false
        }
    }
    
    class func saveEvent(user_id: Int, title: String, text: String, date: String, important: Bool, notifications: AnyObject, list_notifications: AnyObject) -> Bool {
        let context = getContext()
        let entity = NSEntityDescription.entity(forEntityName: "Event", in: context)
        let manageObject = NSManagedObject(entity: entity!, insertInto: context)
        
        // Заполняю новые данные
        manageObject.setValue(user_id, forKey: "user_id")
        manageObject.setValue(title, forKey: "title")
        manageObject.setValue(text, forKey: "text")
        manageObject.setValue(date, forKey: "date")
        manageObject.setValue(important, forKey: "important")
        manageObject.setValue(notifications, forKey: "notifications")
        manageObject.setValue(list_notifications, forKey: "list_notifications")
    
        do {
            try context.save()
            return true
        } catch {
            return false
        }

    }
    
    
//    class func deleteObject(user: User) -> Bool {
//        let context = getContext()
//        context.delete(user)
//        do {
//            try context.save()
//            return true
//        } catch {
//            return false
//        }
//    }
//
//    class func filterData() -> [User]? {
//        let context = getContext()
//        let fetchRequest:NSFetchRequest<User> = User.fetchRequest()
//        var users: [User]? = nil
//
//        let predicate = NSPredicate(format: "password contains[c] %@", "2")
//        fetchRequest.predicate = predicate
//        do {
//            users = try context.fetch(fetchRequest)
//            return users
//        } catch {
//            return users
//        }
//    }
    
}

