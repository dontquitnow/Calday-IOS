//
//  LoginPageController.swift
//  Calday
//
//  Created by d3byte on 27.01.2018.
//  Copyright © 2018 Pandinator. All rights reserved.
//

import UIKit

class LoginPageController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var viewBelowLogin: UIView!
    @IBOutlet weak var phoneInput: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var toProfileSegue: UIButton!
    
    
    
    // MARK: UI Elements
    var passwordInput = UITextField()
    var timerLabel = UILabel()
    var sendSMSButton = UIButton()
    var alertController = UIAlertController()
    
    
    // MARK: Variables
    var timer: Timer!
    var countDownTime = 5
    var loginSubmitted = false
    
    
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setShadow(target: self.loginView)
        setShadow(target: self.passwordView)
        setShadow(target: self.viewBelowLogin)
        self.phoneInput.underlined()
    }
    
    
    
    // MARK: Event handlers
    @IBAction func submit(_ sender: Any) {
        // Эвент указан через Storyboard
        if self.phoneInput.text?.first == "8" && self.phoneInput.text?.count == 11 {
            if !self.loginSubmitted {
                self.moveLoginView()
                self.submitButton.setTitle("ВОЙТИ", for: .normal)
                self.loginSubmitted = true
                self.setTimerLabel()
                self.setSMSButton()
                self.setTimer()
                return
            }
            // Обработать данные и перекинуть в профиль
            self.performSegue(withIdentifier: "goToProfile", sender: self.toProfileSegue)
        } else {
            self.phoneInput.text?.first != "8"
                ? self.alertError(title: "Неправильный формат номера", text: "Измените первую цифру номера на 8")
                : self.alertError(title: "Недостаточно длинный номер", text: "В номере должно быть 11 цифр")
        }
    }
    
    @objc func sendSMS() {
        self.sendSMSButton.isHidden = true
        self.countDownTime = 59
        self.setTimer()
    }
    
    @objc func timerHandler() {
        self.countDownTime -= 1
        self.timerLabel.text = "00:" + (self.countDownTime < 10
            ? ("0" + String(self.countDownTime))
            : String(self.countDownTime))
        if self.countDownTime == 0 {
            self.timer.invalidate()
            self.sendSMSButton.isHidden = false
            self.timerLabel.isHidden = true
        }
    }
    
    
    
    // MARK: Styling functions
    fileprivate func setShadow(target: UIView) {
        target.layer.shadowOpacity = 0.16
        target.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        target.layer.shadowRadius = 8.0
    }
    
    
    
    // MARK: UI Setter Functions
    fileprivate func preparePasswordInput() {
        self.passwordInput.frame = self.phoneInput.frame
        self.passwordInput.textAlignment = .center
        self.passwordInput.placeholder = "Введите пароль"
        self.passwordInput.underlined()
        self.viewBelowLogin.addSubview(self.passwordInput)
    }
    
    fileprivate func setSMSButton() {
        self.sendSMSButton.isHidden = true
        self.sendSMSButton.setTitle("Отправить ещё раз", for: .normal)
        self.sendSMSButton.setTitleColor(UIColor(red:0.26, green:0.27, blue:0.30, alpha:1.0), for: .normal)
        self.sendSMSButton.frame = CGRect(x: self.viewBelowLogin.center.x - 100, y: self.timerLabel.frame.origin.y + 10, width: 200, height: 30)
        self.sendSMSButton.addTarget(self, action: #selector(self.sendSMS), for: .touchUpInside)
        self.viewBelowLogin.addSubview(sendSMSButton)
    }
    
    fileprivate func setTimerLabel() {
        self.timerLabel.text = "00:" + String(self.countDownTime)
        self.timerLabel.textColor = UIColor(red:0.26, green:0.27, blue:0.30, alpha:1.0)
        self.timerLabel.frame = CGRect(x: self.view.center.x - 60, y: self.passwordInput.frame.origin.y + 30, width: 60, height: 30)
        self.viewBelowLogin.addSubview(self.timerLabel)
    }
    
    fileprivate func setTimer() {
        self.timerLabel.isHidden = false
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerHandler), userInfo: nil, repeats: true)
    }
    
    fileprivate func alertError(title: String, text: String) {
        self.alertController = UIAlertController(title: title, message: text, preferredStyle: .alert)
        let action = UIAlertAction(title: "Понятно", style: .default, handler: nil)
        self.alertController.addAction(action)
        self.present(self.alertController, animated: true, completion: nil)
    }
    
    
    
    // MARK: Animation functions
    fileprivate func moveLoginView() {
        UIView.animate(withDuration: 0.4, animations: {
            self.preparePasswordInput()
            self.loginView.frame = CGRect(x: self.loginView.frame.origin.x, y: self.loginView.frame.origin.y + 30, width: self.loginView.frame.width - 70, height: self.loginView.frame.height - 20)
            self.phoneInput.frame = CGRect(x: self.phoneInput.frame.origin.x - 35, y: self.phoneInput.frame.origin.y, width: self.phoneInput.frame.width - 70, height: self.phoneInput.frame.height)
            self.phoneInput.underlined()
            self.phoneInput.textAlignment = .left
        }, completion: {(Bool) in
            self.moveLoginViewBeyondScreen()
        })
    }
    
    fileprivate func moveLoginViewBeyondScreen() {
        UIView.animate(withDuration: 0.3, animations: {
            self.loginView.frame.origin.x += 500
        }, completion: {(Bool) in
            self.loginView.isHidden = true
            UIView.animate(withDuration: 0.3, animations: {
                self.viewBelowLogin.frame = self.passwordView.frame
            })
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UITextField {
    func underlined() {
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor(red:0.56, green:0.58, blue:0.63, alpha:1.0).cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
