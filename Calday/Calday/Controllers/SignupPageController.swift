//
//  SignupPageController.swift
//  Calday
//
//  Created by d3byte on 28.01.2018.
//  Copyright © 2018 Pandinator. All rights reserved.
//

import UIKit

class SignupPageController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var registrationLabel: UILabel!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var firstNameInput: UITextField!
    @IBOutlet weak var lastNameInput: UITextField!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var frameContainer: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var viewBelowNames: UIView!
    @IBOutlet weak var toProfileSegue: UIButton!
    
    
    
    // MARK: UI Elements
    var emailInput = UITextField()
    var phoneInput = UITextField()
    var passwordInput = UITextField()
    var timerLabel = UILabel()
    var sendSMSButton = UIButton()
    var alertController = UIAlertController()
    var topInputFrame = CGRect()
    var bottomInputFrame = CGRect()
    
    
    
    // MARK: Variables
    var timer: Timer!
    var countDownTime = 5
    var nameViewSubmitted = false
    var infoViewSubmitted = false
    
    
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setShadow(target: self.nameView)
        setShadow(target: self.viewBelowNames)
        setShadow(target: self.bottomView)
        self.firstNameInput.underlined()
        self.lastNameInput.underlined()
        self.topInputFrame = self.firstNameInput.frame
        self.bottomInputFrame = self.lastNameInput.frame
        self.phoneInput.placeholder = "Введите свой номер"
        self.phoneInput.textAlignment = .center
        self.phoneInput.keyboardType = .namePhonePad
        self.emailInput.placeholder = "Введите свой email"
        self.emailInput.textAlignment = .center
        self.emailInput.autocapitalizationType = .none
        self.passwordInput.placeholder = "Введите пароль"
        self.passwordInput.textAlignment = .center
    }
    
    
    
    // MARK: Event handlers
    @IBAction func submit(_ sender: Any) {
        // Эвент указан через Storyboard
        if !self.nameViewSubmitted {
            if self.firstNameInput.text?.count == 0 || self.lastNameInput.text?.count == 0 {
                self.alertError(title: "Ошибка", text: "Заполните все поля.")
                return
            }
            self.nameViewSubmitted = true
            self.moveNameView()
        } else if !self.infoViewSubmitted {
            if self.phoneInput.text?.first != "8" || self.phoneInput.text?.count != 11 {
                self.phoneInput.text?.first != "8"
                    ? self.alertError(title: "Неправильный формат номера", text: "Измените первую цифру номера на 8")
                    : self.alertError(title: "Недостаточно длинный номер", text: "В номере должно быть 11 цифр")
                return
            }
            let email = self.emailInput.text ?? ""
            if !self.isValidEmail(email: email) {
                self.alertError(title: "Некорретный email", text: "Введите корректный email адрес")
                return
            }
            self.infoViewSubmitted = true
            self.moveViewBelowNames()
            self.submitButton.setTitle("ЗАРЕГИСТРИРОВАТЬСЯ", for: .normal)
            self.setTimerLabel()
            self.setSMSButton()
            self.setTimer()
        } else {
            // Проверить данные
            self.timer.invalidate()
            self.performSegue(withIdentifier: "passToProfile", sender: self.toProfileSegue)
        }
        
    }
    
    @objc func sendSMS() {
        self.sendSMSButton.isHidden = true
        self.countDownTime = 59
        self.setTimer()
    }
    
    @objc func timerHandler() {
        self.countDownTime -= 1
        self.timerLabel.text = "00:" + (self.countDownTime < 10
            ? ("0" + String(self.countDownTime))
            : String(self.countDownTime))
        if self.countDownTime == 0 {
            self.timer.invalidate()
            self.sendSMSButton.isHidden = false
            self.timerLabel.isHidden = true
        }
    }
    
    
    
    // MARK: Styling functions
    fileprivate func setShadow(target: UIView) {
        target.layer.shadowOpacity = 0.16
        target.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        target.layer.shadowRadius = 8.0
    }
    
    
    
    // MARK: UI Setter Functions
    fileprivate func preparePasswordInput() {
        self.passwordInput.frame = CGRect(x: self.bottomView.center.x, y: self.bottomView.center.y, width: 259, height: 30)
        self.passwordInput.textAlignment = .center
        self.passwordInput.placeholder = "Введите пароль"
        self.passwordInput.underlined()
        self.bottomView.addSubview(self.passwordInput)
    }
    
    fileprivate func setSMSButton() {
        self.sendSMSButton.isHidden = true
        self.sendSMSButton.setTitle("Отправить ещё раз", for: .normal)
        self.sendSMSButton.setTitleColor(UIColor(red:0.26, green:0.27, blue:0.30, alpha:1.0), for: .normal)
        self.sendSMSButton.frame = CGRect(x: self.bottomView.center.x - 100, y: self.bottomView.center.y + 10, width: 200, height: 30)
        self.sendSMSButton.addTarget(self, action: #selector(self.sendSMS), for: .touchUpInside)
        self.bottomView.addSubview(self.sendSMSButton)
    }
    
    fileprivate func setTimerLabel() {
        self.timerLabel.text = "00:" + String(self.countDownTime)
        self.timerLabel.textColor = UIColor(red:0.26, green:0.27, blue:0.30, alpha:1.0)
        self.timerLabel.frame = CGRect(x: self.bottomView.center.x - 30, y: self.bottomView.center.y + 10, width: 60, height: 30)
        self.bottomView.addSubview(self.timerLabel)
    }
    
    fileprivate func setTimer() {
        self.timerLabel.isHidden = false
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerHandler), userInfo: nil, repeats: true)
    }
    
    fileprivate func alertError(title: String, text: String) {
        self.alertController = UIAlertController(title: title, message: text, preferredStyle: .alert)
        let action = UIAlertAction(title: "Понятно", style: .default, handler: nil)
        self.alertController.addAction(action)
        self.present(self.alertController, animated: true, completion: nil)
    }
    
    fileprivate func setTextFields(target: UIView, textFields: [UITextField]? = nil) {
        if let tFields = textFields {
            if tFields.count == 2 {
                tFields[0].frame = self.topInputFrame
                tFields[1].frame = self.bottomInputFrame
            } else if tFields.count == 1 {
                tFields[0].frame = CGRect(x: target.center.x - self.topInputFrame.width / 2, y: target.center.y - 30, width: self.topInputFrame.width, height: 30)
            }
            for tField in tFields {
                tField.underlined()
                target.addSubview(tField)
            }
        }
    }
    
    
    
    // MARK: Animation functions
    fileprivate func moveNameView() {
        UIView.animate(withDuration: 0.4, animations: {
            self.nameView.frame = CGRect(x: self.nameView.frame.origin.x, y: self.nameView.frame.origin.y + 30, width: self.nameView.frame.width - 70, height: self.nameView.frame.height - 20)
            self.firstNameInput.frame = CGRect(x: self.firstNameInput.frame.origin.x - 35, y: self.firstNameInput.frame.origin.y, width: self.firstNameInput.frame.width - 70, height: self.firstNameInput.frame.height)
            self.firstNameInput.textAlignment = .left
            self.lastNameInput.frame = CGRect(x: self.lastNameInput.frame.origin.x - 35, y: self.lastNameInput.frame.origin.y, width: self.lastNameInput.frame.width - 70, height: self.lastNameInput.frame.height)
            self.lastNameInput.textAlignment = .left
        }, completion: {(Bool) in
            self.moveViewBeyondScreen(target: self.nameView, nextView: self.viewBelowNames, lastView: self.bottomView, textFields: [self.emailInput, self.phoneInput])
        })
    }
    
    fileprivate func moveViewBelowNames() {
        UIView.animate(withDuration: 0.4, animations: {
            self.viewBelowNames.frame = CGRect(x: self.viewBelowNames.frame.origin.x, y: self.viewBelowNames.frame.origin.y + 30, width: self.viewBelowNames.frame.width - 70, height: self.viewBelowNames.frame.height - 20)
            self.emailInput.frame = CGRect(x: self.emailInput.frame.origin.x - 35, y: self.emailInput.frame.origin.y, width: self.emailInput.frame.width - 70, height: self.emailInput.frame.height)
            self.emailInput.textAlignment = .left
            self.phoneInput.frame = CGRect(x: self.phoneInput.frame.origin.x - 35, y: self.phoneInput.frame.origin.y, width: self.phoneInput.frame.width - 70, height: self.phoneInput.frame.height)
            self.phoneInput.textAlignment = .left
        }, completion: {(Bool) in
            self.moveViewBeyondScreen(target: self.viewBelowNames, nextView: self.bottomView, textFields: [self.passwordInput])
        })
    }
    
    fileprivate func moveViewBeyondScreen(target: UIView, nextView: UIView, lastView: UIView? = nil, textFields: [UITextField]?) {
        UIView.animate(withDuration: 0.3, animations: {
            target.frame.origin.x += 500
        }, completion: {(Bool) in
            self.setTextFields(target: nextView, textFields: textFields)
            target.isHidden = true
            UIView.animate(withDuration: 0.3, animations: {
                lastView?.frame = nextView.frame
                nextView.frame = self.frameContainer.frame
            })
        })
    }
    
    
    
    // MARK: Other
    func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }

}
